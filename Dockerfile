FROM golang:alpine
RUN apk add --update git

WORKDIR $GOPATH/src/github.com/terraform-providers/terraform-provider-kubernetes/
RUN git clone https://github.com/rmb938/terraform-provider-kubernetes.git ./ && \
  git checkout a23a10437ebedff0e92d97d5f50a0369f681ce49 && \
  CGO_ENABLED=0 go build -o bin/terraform-provider-kubernetes

WORKDIR $GOPATH/src/github.com/terraform-providers/terraform-provider-helm/
RUN git clone https://github.com/rmb938/terraform-provider-helm.git ./ && \
  git checkout ea4d51e67cf52b529170d8f78da1bee9bb3aab6d && \
  CGO_ENABLED=0 go build -o bin/terraform-provider-helm

FROM hashicorp/terraform:0.11.11

ENV SOPS_VERSION=3.2.0

COPY --from=0 /go/src/github.com/terraform-providers/terraform-provider-kubernetes/bin/terraform-provider-kubernetes /root/.terraform.d/plugins/
COPY --from=0 /go/src/github.com/terraform-providers/terraform-provider-helm/bin/terraform-provider-helm /root/.terraform.d/plugins/
RUN curl -L https://github.com/mozilla/sops/releases/download/${SOPS_VERSION}/sops-${SOPS_VERSION}.linux -o /bin/sops && chmod +x /bin/sops
